# Package Local Publisher

```
Usage: package-local-publisher [options]

Build and deploy npm package on filesystem

Options:
  -V, --version                  output the version number
  -s, --source <path>            Path to root directory of source project director (default: "current working directory")
  -d, --dist <path>              Path to directory witch contains build of source project, relative to source root directory
  -t, --target <path>            Path to directory in node_modules of target project director, like target_project/node_modules/source_project
  -w, --watch                    Watch changes (default: false)
  -b, --build-command [command]  Command for build source project (default: "npm run package")
  -h, --help                     output usage information
```
