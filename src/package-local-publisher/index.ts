#!/usr/bin/env node

const { watchTree } = require('watch');
const { copySync } = require("cpx");
const { execSync } = require('child_process');
const chalk = require('chalk');
const clear = require('clear');
const { textSync } = require('figlet');
const { join, resolve } = require('path');
const program = require('commander');
const { version } = require('../../package.json');

function build(directory: string, command: string, dist: string, target: string) {
    console.log('Building...');
    execSync(command, {
        cwd: directory
    });
    console.log('\nCopying files...');
    copySync(`${dist}/**/*`, target)
}

clear();
console.log(
    chalk.blue(
        textSync('package-local-publisher', { horizontalLayout: 'full' })
    )
);

program
    .version(version)
    .description('Build and deploy npm package on filesystem')
    .option('-s, --source <path>', 'Path to root directory of source project director', process.cwd())
    .requiredOption('-d, --dist <path>', 'Path to directory witch contains build of source project, relative to source root directory')
    .requiredOption('-t, --target <path>', 'Path to directory in node_modules of target project director, like target_project/node_modules/source_project')
    .option('-w, --watch', 'Watch changes', false)
    .option('-b, --build-command [command]', 'Command for build source project', 'npm run package')
    .parse(process.argv);


const sourceAbsolutePath = resolve(program.source);
const distAbsolutePath = join(sourceAbsolutePath, program.dist);
const targetAbsolutePath = resolve(program.target);

console.log('source:', sourceAbsolutePath);
console.log('dist:', distAbsolutePath);
console.log('target:', targetAbsolutePath);
console.log('command:', program.buildCommand);
console.log('watch:', program.watch);

if (program.watch) {
    watchTree(sourceAbsolutePath, {
        ignoreDirectoryPattern: new RegExp(`${distAbsolutePath}|${join(sourceAbsolutePath, 'node_modules')}`, 'g')
    }, () => {
        build(sourceAbsolutePath, program.buildCommand, distAbsolutePath, targetAbsolutePath);
    });
} else {
    build(sourceAbsolutePath, program.buildCommand, distAbsolutePath, targetAbsolutePath);
}
